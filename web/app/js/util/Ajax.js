/**
 * 
 */
class Ajax {
    /**
     * STATIC
     * @param {*} text 
     */
    static parse(text){
        try {
            return JSON.parse(text);
        } catch(e){
            return text;
        }
    }

    /**
     * STATIC
     * @param {*} type 
     * @param {*} url 
     * @param {*} data 
     * @param {*} options 
     */
    static xhr(type, url, data, options) {
        options = options || {};

        var request = new XMLHttpRequest();
        
        request.open(type, url, true);
        
        if(type === "POST"){
            request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        }
        
        request.onreadystatechange = function () {
            if (this.readyState === 4) {
                if (this.status >= 200 && this.status < 400) {
                    console.log(this);
                    options.success && options.success(Ajax.parse(this.responseText));
                } else {
                    options.error && options.error(this.status);
                }
            }
        };
        request.send(data);
    }

    /**
     * STATIC
     * @param {*} method 
     * @param {*} url 
     * @param {*} data 
     * @param {*} callback 
     */
    static ajax(method, url, data, callback) {
        return Ajax.xhr(method, url, data, {success:callback});
    }

    /**
     * STATIC
     * @param {*} url 
     * @param {*} callback 
     */
    static get(url, callback) {
        return Ajax.xhr("GET", url, undefined, {success:callback});
    }

    /**
     * STATIC
     * @param {*} url 
     * @param {*} data 
     * @param {*} callback 
     */
    static post(url, data, callback) {
        return Ajax.xhr("POST", url, data, {success:callback});
    }

    /**
     * STATIC
     * @param {*} url 
     * @param {*} data 
     * @param {*} callback 
     */
    static put(url, data, callback) {
        return Ajax.xhr("PUT", url, data, {success:callback});
    }
}

export default Ajax;
