/**
 * 
 * @param {*} container 
 * @param {*} eventName 
 * @param {*} targetSelector 
 * @param {*} handler 
 */
function delegateEvent(container, eventName, targetSelector, handler) {
    container.addEventListener(eventName, (event) => {
        let target = event.target;
       
        while (target !== container && !matches(target, targetSelector)) {
            target = target.parentNode;
        }

        if (target !== container) {
            event.delegateTarget = target;
            handler(event);
        }
    });
}

/**
 * 
 * @param {*} element 
 * @param {*} selector 
 */
function matches(element, selector) {
    return element.matches(selector);
}

export default delegateEvent;
