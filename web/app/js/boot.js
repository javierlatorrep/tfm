import PanelSelectorView from './view/PanelSelectorView';
import ChatView from './view/ChatView';
import MainAppView from './view/MainAppView';
import SearchUserView from './view/SearchUserView';
import SearchUserModel from './model/SearchUserModel';
import SearchUserController from './controller/SearchUserController';
import ChatController from './controller/ChatController';
import ChatModel from './model/ChatModel';
import AppView from './view/AppView';
import AppController from './controller/AppController';

const searchUserModel = new SearchUserModel();

const appPanel      = document.getElementById('main-app'),
      callPanel     = document.getElementById('main-call'),
      endCallButton = document.getElementById('end-call-button'),
      localVideo    = document.getElementById('local-video'),
      remoteVideo   = document.getElementById('remote-video');

const mainAppView = new MainAppView(
    appPanel,
    callPanel,
    endCallButton,
    localVideo,
    remoteVideo
);

const activePanel       = document.getElementById('search-users-wrapper'),
      leftPanel         = document.getElementById('left-panel'),
      showMenuButton    = document.getElementById('left-panel-mobile-menu-show-btn'),
      appView           = new AppView(activePanel, leftPanel, showMenuButton);

const selectorsWrapper = document.getElementById('app-panel-selector');
const panelSelectorView = new PanelSelectorView({ selectorsWrapper: selectorsWrapper });

const chat = document.getElementById('js-chat'),
      callButton = document.getElementById('chat-call-button');
const chatView = new ChatView(chat, callButton);
const chatModel = new ChatModel();

const searchUserWrapper = document.getElementById('search-users-wrapper');
const searchUserView = new SearchUserView(searchUserWrapper);

const username = document.getElementById('username').getAttribute("data-username");

const appController = new AppController(
    appView,
    panelSelectorView,
    searchUserView,
    searchUserModel,
    chatView,
    chatModel,
    username
);

appController.start();


const ChatControllerNew = new ChatController(
    appController,
    mainAppView,
    appView,
    chatView,
    chatModel,
    searchUserModel,
    username
);

const searchUserController = new SearchUserController(
    appController,
    searchUserModel,
    searchUserView,
    chatView,
    chatModel,
    username
);
searchUserController.start();

window.appController = appController;
