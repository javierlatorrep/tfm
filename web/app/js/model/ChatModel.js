import Ajax from "../util/Ajax";

/**
 * 
 */
class ChatModel {
    /**
     * 
     * @param {*} localUsername 
     * @param {*} connectedUsername 
     * @param {*} callback 
     */
    getChatDatesBetweenUsers(localUsername,connectedUsername, callback){
        console.log( " Getting chat dates between ",localUsername, " and " ,connectedUsername);

        const url = Routing.generate(
            "getChatDatesBetweenUsers",
            true
        );
        Ajax.post(
            url,
             "&localUsername=" + encodeURIComponent(localUsername) + "&connectedUsername=" + encodeURIComponent(connectedUsername) ,
            (data) => {
                callback(data);
            }
        );
    }

    /**
     * 
     * @param {*} localUsername 
     * @param {*} connectedUsername 
     * @param {*} callback 
     */
    openChat(localUsername,connectedUsername, callback){
        console.log("Open chat between " ,localUsername, " and " , connectedUsername);

        const url = Routing.generate(
            "openChat",
            true
        );
        Ajax.post(
            url,
             "localUsername=" + encodeURIComponent(localUsername) + "&connectedUsername=" + encodeURIComponent(connectedUsername) ,
            (data) => {
                callback(data);
             }
        );
    }

    /**
     * 
     * @param {*} chatId 
     * @param {*} message 
     * @param {*} localUsername 
     * @param {*} connectedUsername 
     * @param {*} callback 
     */
    save(chatId,message,localUsername,connectedUsername, callback){
        console.log( "chat ID=>", chatId, " Saving message '", message,"' between ",localUsername, " and " ,connectedUsername);

        const url = Routing.generate(
            "saveChatMessage",
            true
        );
        Ajax.post(
            url,
            "chatId=" + chatId + "&message=" + message + "&localUsername=" + encodeURIComponent(localUsername) + "&connectedUsername=" + encodeURIComponent(connectedUsername) ,
            (data) => {
                callback(data);
            }
        );
    }

    /**
     * 
     * @param {*} localUsername
     * @param {*} connectedUsername
     * @param {*} callback
     */
    setActiveUser(){
        const url = Routing.generate(
            "activeUser",
            null,
            true
        );

        Ajax.get(
            url,
            (data) => {
                console.log(data);
            }
        );
    }

    /**
     *
     * @param {*} localUsername
     * @param {*} callback
     */
    getUsersIHaveChattedWith(localUsername, callback){
        console.log( " Getting users ", localUsername , " has chatted with ");

        const url = Routing.generate(
            "getUsersIHaveChattedWith",
            true
        );
        Ajax.post(
            url,
            "&localUsername=" + encodeURIComponent(localUsername) ,
            (data) => {
                callback(data);
            }
        );
    }

    /**
     *
     * @param {*} localUsername
     * @param {*} remoteUsername
     * @param {*} callback
     */
    getChatsBetweenUsers(localUsername,remoteUsername,callback){
        console.log( " Getting chats between local user ", localUsername , " and remote user ", remoteUsername);

        const url = Routing.generate(
            "getChatsBetweenUsers",
            true
        );
        Ajax.post(
            url,
            "&localUsername=" + encodeURIComponent(localUsername) + "&remoteUsername=" + encodeURIComponent(remoteUsername) ,
            (data) => {
                callback(data);
            }
        );
    }
}

export default ChatModel;
