import Ajax from "../util/Ajax";

/**
 * 
 */
class SearchUserModel {
    /**
     * 
     * @param {*} query 
     * @param {*} callback 
     */
    searchUsers(query,callback) {
        const url = Routing.generate(
                        "users",
                        {query : query},
                        true
                    );
        Ajax.get(url, (users) => {
            callback(users);
        });
    }

    /**
     * 
     * @param {*} id 
     * @param {*} callback 
     */
    find(id, callback) {
        const url = Routing.generate(
            "findUser",
            {id: id},
            true
        );
        Ajax.get(url, (data) => {
            callback(data);
        });
    }

    /**
     * 
     * @param {*} email 
     * @param {*} callback 
     */
    findByEmail(email, callback) {
        const url = Routing.generate(
            "findUserByEmail",
            {email: email},
            true
        );
        Ajax.get(url, (data) => {
            callback(data);
        });
    }
}

export default SearchUserModel;
