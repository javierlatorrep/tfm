import { EventEmitter } from 'events';

/**
 * 
 */
class MainAppView extends EventEmitter {
    /**
     * 
     * @param {*} appPanel 
     * @param {*} callPanel 
     * @param {*} endCallButton 
     * @param {*} localVideo 
     * @param {*} remoteVideo 
     */
    constructor(appPanel, callPanel, endCallButton, localVideo, remoteVideo) {
        super();

        this.activePanel    = appPanel;
        this.appPanel       = appPanel;
        this.callPanel      = callPanel;
        this.endCallButton  = endCallButton;
        this.localVideo     = localVideo;
        this.remoteVideo    = remoteVideo;

        this.endCallButton.addEventListener("click", (evt, endCallButton) => {
            evt.stopPropagation();
            
            this.emit("end_call");
        });
    }

    /**
     * 
     */
    _hideActivePanel () {
        this.activePanel.style.display = "none";
    }

    /**
     * 
     * @param {*} panel 
     */
    _showPanel(panel) {
        if (this.activePanel !== panel) {
            if (this.activePanel)
                this._hideActivePanel();

            this.activePanel = panel;
            this.activePanel.style.display = "block";
        }
    }

    /**
     * 
     */
    showAppPanel() {
        this._showPanel(this.appPanel);
    }

    /**
     * 
     */
    showCallPanel() {
        this._showPanel(this.callPanel);
    }

    /**
     * 
     * @param {*} src 
     */
    setLocalVideoSrcObject(src) {
        this.localVideo.srcObject = src;
    }

    /**
     * 
     * @param {*} src 
     */
    setRemoteVideoSrcObject(src) {
        this.remoteVideo.srcObject = src;
    }
}

export default MainAppView;
