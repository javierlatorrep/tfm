import { EventEmitter } from 'events';
import htmlToElement from '../util/htmlToElement';
import delegateEvent from '../util/delegateEvent';

/**
 * 
 */
class AppView extends EventEmitter{
    /**
     * CONSTANTS
     */
    static get CLASS_BOOTSTRAP_HIDDEN_XS() { return "hidden-xs" }
    static get ATTRIBUT_DATA_USER_ID() { return "data-user-id" }
    static get ID_CHAT_HISTORY() { return "#chat-history" }
    static get CLASS_SHOW_CHAT_HISTORY_USER() { return "show-chat-history-user" }
    static get CHAT_HISTORY_USER_SELECTED() { return "chatHistoryUserSelected" }
    /**
     * STATIC
     * @param {*} status
     */
    static getUserStatusTemplate(status) {
        return status ?
            "<span class='fa fa-circle text-success'></span> Connected" :
            "<span class='fa fa-circle text-danger'></span> Disconnected";
    }
    /**
     * 
     * @param {*} activePanel 
     * @param {*} leftPanel 
     * @param {*} showMenuButton 
     */
    constructor(activePanel, leftPanel, showMenuButton) {

        super();
        this._activePanel       = null;
        this._leftPanel         = leftPanel;
        this._showMenuButton    = showMenuButton;
        this._chatHistory       = this._leftPanel.querySelector("#chat-history");

        this.showPanel(activePanel);

        this._showMenuButton.addEventListener("click", (evt) => {
            evt.preventDefault();

            const showMenuButton = evt.currentTarget;

            if (this._leftPanel.classList.contains(AppView.CLASS_BOOTSTRAP_HIDDEN_XS))
                this._leftPanel.classList.remove(AppView.CLASS_BOOTSTRAP_HIDDEN_XS);
            else
                this._leftPanel.classList.add(AppView.CLASS_BOOTSTRAP_HIDDEN_XS);
        });
    }

    /**
     * 
     */
    _hideActivePanel () {
        this._activePanel.style.display = "none";
    }

    /**
     * 
     * @param {*} panel 
     */
    showPanel(panel) {
        if (this._activePanel !== panel) {
            if (this._activePanel)
                this._hideActivePanel();

            this._activePanel = panel;
            this._activePanel.style.display = "block";

        }
    }

    /**
     * Lista los usuarios con los que se ha establecido algún chat
     * @param {Object[]} [users =[]]
     */
    listChatHistory(users = []) {
        console.log(users);
        this._chatHistory.innerHTML = '';

        if (users.length)
            users.forEach(user => this._chatHistory.appendChild(AppView.renderChatUser(user)))
        else
            this._chatHistory.appendChild(document.createTextNode("No chats yet."));
    }

    /**
     * Lanza evento USER_SELECTED cuando se hace clic sober un usuario.
     */
    _setupChatHistory() {
        delegateEvent(this._chatHistory, "click", "." + AppView.CLASS_SHOW_CHAT_HISTORY_USER, (event) => {
            const remote_user_id = event.delegateTarget.getAttribute(AppView.ATTRIBUT_DATA_USER_ID);
            this.emit(AppView.CHAT_HISTORY_USER_SELECTED, remote_user_id);
        });
    }

                /**
     * STATIC
     * Devuelve un elemento HTML que representa el usuario
     * @param  {Object} user
     * @param  {String} date
     * @return {Element}
     */
    static renderChatUser(user) {
        console.log(user);
        return htmlToElement(` 
            <li class="${AppView.CLASS_SHOW_CHAT_HISTORY_USER}" data-user-id="${user.id}">
                <div class="media">
                    <div class="media-left">
                        <span class="glyphicon glyphicon-user user-avatar show-chat-history-user-avatar"</span>
                    </div>
                    <div class="media-body show-chat-history-user-name">
                        <div data-username="${user.email}" class="media-heading ">${user.email}</div>
                        <div class="show-chat-history-user-status">${AppView.getUserStatusTemplate(user.active)}</div>
                    </div>
                </div>
            </li>        
        `);
    }
}

export default AppView;
