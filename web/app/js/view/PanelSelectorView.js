import { EventEmitter } from "events";
import delegateEvent from "../util/delegateEvent";

/**
 * 
 */
class PanelSelectorView extends EventEmitter {
    /**
     * CONSTANTS
     */
    static get ATTRIBUT_DATA_PANEL() { return "data-panel" }
    static get CLS_ACTIVE_ICON() { return "active" }
    static get CLS_ICON() { return "app-panel-selector-icon" }
    static get EVENT_PANEL_SELECTED() { return "panelSelected" }

    /**
     * 
     * @param {*} param0 
     */
    constructor({ selectorsWrapper }) {
        super();

        this.selectorsWrapper = selectorsWrapper;
    }

    /**
     * 
     */
    start() {
        this._activeSelector = this.selectorsWrapper.querySelector("." + PanelSelectorView.CLS_ACTIVE_ICON);
        this._setupRightPanelSelection();
    }

    /**
     * 
     */
    getSelectorsWrapper() {
        return this.selectorsWrapper;
    }

    /**
     * 
     */
    _setupRightPanelSelection() {
        delegateEvent(this.selectorsWrapper, "click", "." + PanelSelectorView.CLS_ICON, (event) => {
            const id = event.delegateTarget.getAttribute(PanelSelectorView.ATTRIBUT_DATA_PANEL),
                  icon = event.delegateTarget;

            if (this._activeSelector !== icon) {
                const panel = document.querySelector("#" + id);
                this.emit(PanelSelectorView.EVENT_PANEL_SELECTED, panel);
                this._unmarkActiveSelector();
                this._markSelectorAsActive(icon);
            }
        });
    }

    /**
     * 
     * @param {*} icon 
     */
    _markSelectorAsActive(icon) {
        this._activeSelector = icon;
        this._activeSelector.classList.add(PanelSelectorView.CLS_ACTIVE_ICON);
    }

    /**
     * 
     */
    _unmarkActiveSelector() {
        this._activeSelector.classList.remove(PanelSelectorView.CLS_ACTIVE_ICON);
    }
}

export default PanelSelectorView;
