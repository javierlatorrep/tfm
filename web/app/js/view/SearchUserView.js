import { EventEmitter } from 'events';
import htmlToElement from '../util/htmlToElement';
import delegateEvent from '../util/delegateEvent';

/**
 * 
 */
class SearchUserView extends EventEmitter {
    /**
     * CONSTANTS
     */
    static get ATTRIBUT_DATA_USER_ID() { return "data-user-id" }
    static get ATTRIBUT_DATA_USER_STATUS() { return "data-user-status" }
    static get ESCAPE_KEYCODE() { return 27 }
    static get ID_SEARCH_USER() { return "js-search-user" }
    static get CLS_ACTIVE_ICON() { return "active" }
    static get CLASS_SHOW_USER() { return "show-user" }
    static get USER_SELECTED() { return "userSelected" }
    static get INPUT_VALUE_CHANGE() { return "inputValueChange" }
    static get EVENT_PANEL_SELECTED() { return "panelSelected" }
    static get TEMPLATE_SPINNER() { return "<div class='fa fa-refresh fa-spin app-spinner' />" }

    /**
     * STATIC
     * Devuelve un elemento HTML que representa el usuario
     * @param  {Object} user
     * @return {Element}
     */
    static renderUser(user) {
        return htmlToElement(` 
            <li class="${SearchUserView.CLASS_SHOW_USER} col-sm-12 col-md-6 col-lg-4" data-user-id="${user.id}" data-user-status= "${user.active}">
                <div class="media">
                    <div class="media-left">
                        <span class="glyphicon glyphicon-user user-avatar search-user-avatar"</span>
                    </div>
                    <div class="media-body user-name">
                        <div data-username="${user.email}" class="media-heading search-user-name">${user.email}</div>
                        <div  class="search-user-status">${SearchUserView.getUserStatusTemplate(user.active)}</div>
                    </div>
                </div>
            </li>        
        `);
    }

    /**
     * STATIC
     * @param {*} status 
     */
    static getUserStatusTemplate(status) {
        return status ? 
            "<span class='fa fa-circle text-success'></span> Connected" :
            "<span class='fa fa-circle text-danger'></span> Disconnected";
    }

    /**
     * 
     * @param {*} element 
     */
    constructor(element) {
        super();

        this._element = element;
        this._lastQuery = "";
    }

    /**
     * 
     */
    getSearchInput() {
        return this._element.querySelector("input");
    }

    /**
     * 
     */
    getSearchResultWrapper() {
        return  this._element.querySelector(".app-panel-content");
    }

    /**
     * Resetea el listado con los usuarios.
     * @param {Object[]} [users =[]]
     */
    listUsers(users = []) {
        this.getSearchResultWrapper().innerHTML = '';

        if (users.length)
            users.forEach((user) => this.getSearchResultWrapper().appendChild(SearchUserView.renderUser(user)))
        else
            this.getSearchResultWrapper().appendChild(document.createTextNode("No results."));
    }

    /**
     * Lanza evento keyup cuando se escribe en input
     */
    _setupInputValueChange() {
        const searchUserInput = this.getSearchInput();

        searchUserInput.addEventListener("keyup", (evt) => {
            evt.preventDefault();
            
            if (evt.which === SearchUserView.ESCAPE_KEYCODE)
                searchUserInput.value = "";
            
            var searchQuery = searchUserInput.value.trim();

            if (searchQuery && searchQuery !== this._lastQuery) {
                this.getSearchResultWrapper().innerHTML = SearchUserView.TEMPLATE_SPINNER
                this.emit(SearchUserView.INPUT_VALUE_CHANGE, searchQuery);
            } else if (searchQuery === "")
                this.listUsers();

            this._lastQuery = searchQuery;
        });
    }

    /**
     * Lanza evento USER_SELECTED cuando se hace clic sobre un usuario.
     */
    _setupUserSelection() {
        delegateEvent(this._element, "click", "." + SearchUserView.CLASS_SHOW_USER, (event) => {
            const id = event.delegateTarget.getAttribute(SearchUserView.ATTRIBUT_DATA_USER_ID),
                icon = event.delegateTarget;
            const status = event.delegateTarget.getAttribute(SearchUserView.ATTRIBUT_DATA_USER_STATUS);

            if(status === 'true'){
                if (this._activeSelector !== icon) {
                    this.emit(SearchUserView.USER_SELECTED, id);
                }
            }
            else{
                alert("User not connected! Not possible to open channel!!");
            }

        });
    }
}

export default SearchUserView;
