import { EventEmitter } from 'events';
import htmlToElement from '../util/htmlToElement';
import delegateEvent from '../util/delegateEvent';

/**
 * 
 */
class ChatView extends EventEmitter {
    /**
     * CONSTANTS
     */
    static get ID_CHAT() { return "js-chat" }
    static get ATTRIBUT_DATA_USER_ID() { return "data-chat-id" }
    static get CLS_ACTIVE_ICON() { return "active" }
    static get CLS_APP_BUTTON_DISABLED() { return "app-button-disabled" }
    static get USER_SELECTED() { return "userSelected" }
    static get CLS_LOCAL_MESSAGE() { return "local-message" }
    static get CLS_REMOTE_MESSAGE() { return "remote-message" }
    static get ENTER_KEY_CODE() { return 13 }
    static get CHAT_CALL_BUTTON_ID() { return "chat-call-button"}

    /**
     * STATIC
     * @param {*} status 
     */
    static getUserStatusTemplate(status) {
        return status ? 
            "<span class='fa fa-circle text-success'></span> Connected" :
            "<span class='fa fa-circle text-danger'></span> Disconnected";
    }
    
    /**
     * 
     * @param {*} _element 
     * @param {*} callButton 
     */
    constructor(_element, callButton) {
        super();
        
        this._element = _element;
        this.callButton = callButton;

        this._setupCallButton();
    }

    /**
     * 
     * @param {*} message 
     * @param {*} isRemoteMessage
     */
    addMessage(message, isRemoteMessage) {
        var messageClass = isRemoteMessage ?
            ChatView.CLS_REMOTE_MESSAGE : 
            ChatView.CLS_LOCAL_MESSAGE;
        this.getChatMessageWrapper().appendChild(renderMessage(message, messageClass));
    }

    /**
     * 
     */
    disableSendButton() {
        const sendButton= this.getSendButton();
        sendButton.setAttribute("disabled", true);
        sendButton.classList.add(ChatView.CLS_APP_BUTTON_DISABLED);
    }

    /**
     *
     */
    getChatContactWrapper() {
        return this._element.querySelector("#chat-contact-wrapper");
    }

    /**
     *
     */
    getChatMessageWrapper() {
        return this._element.querySelector("#chat-message-wrapper");
    }

    /**
     *
     */
    getContactName() {
        return this._element.querySelector("#chat-contact-name");
    }

    /**
     *
     */
    getContactStatus(){
        return this._element.querySelector("#chat-contact-status");
    }

    /**
     *
     */
    getChatInput(){
        return this._element.querySelector("#chat-input input");
    }

    /**
     *
     */
    hideCallButton(){
        return this._element.querySelector("#chat-call-button").style.visibility = "hidden";
    }

    /**
     *
     */
    hideChatInput(){
        this._element.querySelector("#chat-input").style.visibility = "hidden";
    }

    /**
     *
     */
    showChatInput(){
        this._element.querySelector("#chat-input").style.visibility = "";
    }

    /**
     * deleteChatMessageWrapper
     */
    deleteChatMessageWrapper(){
        this.getChatMessageWrapper().innerHTML= "";
    }

    /**
     * 
     */
    getSendButton(){
        return this._element.querySelector("#chat-input button");
    }

    /**
     * 
     * @param {*} user 
     */
    showContact(user){
        var name    = this.getContactName(),
            active  = this.getContactStatus();
        name.firstChild.nodeValue = user.email;
        active.innerHTML = ChatView.getUserStatusTemplate(user.active);
    }

    /**
     * 
     */
    resetInputValue() {
        this.getChatInput().value = "";
    }

    /**
     * Lanza evento keyup cuando se escribe en input
     */
    _setupInputMessage() {

       const sendButton= this.getSendButton();

        sendButton.addEventListener("click", (evt) => {
            evt.preventDefault();

            var input = this.getChatInput();
            if (input) {
                var message = input.value;
                this.emit("message_sent", message  );
            }
        });
    }

    /**
     * 
     */
   _setupInputValueChange (){
        const sendButton= this.getSendButton();
        const message = this.getChatInput();

        message.addEventListener("keyup", (evt) => {
            evt.preventDefault();

            if (message.value) {
                if (evt.which === ChatView.ENTER_KEY_CODE) {
                    sendButton.click();
                } else {
                    sendButton.removeAttribute("disabled");
                    sendButton.classList.remove(ChatView.CLS_APP_BUTTON_DISABLED);
                }
            } else {
                this.disableSendButton();
            }
        });
    } 

    /**
     * 
     */
    _setupCallButton(){
        this.callButton.addEventListener("click", (evt, sendButton) => {
            evt.preventDefault();
            this.emit("start_call");
        });
    }
}

/**
 * Devuelve un _elemento HTML que representa un mensaje
 * @param  {Object} user
 * @return {Element}
 */
function renderMessage(message, messageWrapperClass) {
    return htmlToElement(`
        <li class='${messageWrapperClass}'>
            <span>${message}</span>
        </li>
    `);
}

export default ChatView;
