import { EventEmitter } from 'events';

/**
 * 
 */
class AppController extends EventEmitter {
    /**
     * 
     * @param {*} appView 
     * @param {*} panelSelectorView 
     * @param {*} chatView 
     * @param {*} searchUserView 
     */
    constructor(appView, panelSelectorView, searchUserView, searchUserModel, chatView, chatModel, username) {
        super();

        this._appView = appView;
        this._panelSelectorView = panelSelectorView;
        this._searchUserView = searchUserView;
        this._searchUserModel = searchUserModel;
        this._chatView = chatView;
        this._chatModel = chatModel;
        this._username  = username;
    }

    /**
     * 
     */
    start() {
        this._setupPanelSelection();
        this._setupUserView();
        this._setupChatHistory();
    }

    /**
     * 
     */
    _setupPanelSelection() {
        this._panelSelectorView.on('panelSelected', (data) => this._onPanelSelected(data));
        this._panelSelectorView.start();
    }

    /**
     * Configura la vista de chat de un usuario
     * @private
     */
    _setupUserView() {
        this._searchUserView.on('panelSelected', (data) => this._onUserPanelSelected(data));
    }

    /**
     * Configura la vista de chat de un usuario
     * @private
     */
    _setupChatHistory() {
        this._showUsersIHaveChattedWith();
        this._appView._setupChatHistory();
        this._appView.on('chatHistoryUserSelected', (data) => this._onChatHistoryUserSelected(data));
    }

    /**
     * 
     * @param {*} panel 
     */
    _onPanelSelected(panel) {
        this._appView.showPanel(panel);
    }

    /**
     * 
     * @param {*} panel 
     */
    _onUserPanelSelected(panel) {
        this._appView.showPanel(panel);
    }

    /**
     * Gestiona la selección de un usuario para abrir panel de chat
     * @param userId
     */
    _onChatHistoryUserSelected(userId) {

        console.log(userId);
        this._showChatsWith(userId);
        this._searchUserModel.find(userId, (userstr) => {
            var user = userstr;

            if (typeof user === 'string') {
                user = JSON.parse(user)[0];
            }
            if (user && user.active) {
                this.emit('create_offer', user);
            }
        });

    }

    /**
     * muestra usuarios con los que se ha chateado
     */
    _showUsersIHaveChattedWith() {

        this._chatModel.getUsersIHaveChattedWith(this._username, (users) => {
            console.log(users);
            this._appView.listChatHistory(JSON.parse(users));
        });
    }

    _showChatsWith(remoteUserId){

        this._appView.showPanel(document.querySelector("#js-chat"));
        this._chatView.deleteChatMessageWrapper();
        this._searchUserModel.find(remoteUserId, (userstr) => {
            var remoteUser = userstr;

            if (typeof remoteUser === 'string') {
                remoteUser = JSON.parse(remoteUser)[0];
            }

            this._chatView.showContact(remoteUser);

            if(!remoteUser.active){
                this._chatView.hideCallButton();
                this._chatView.hideChatInput();
            }
            this._chatModel.getChatsBetweenUsers(this._username, remoteUser.email, (data) => {
                var messages = JSON.parse(data);

                messages.forEach((message) => {
                    //console.log(">>",message.message, ">>tx_id", message.transmitter_id, ">>rx_id", remoteUserId);
                    if(message.transmitter_id == remoteUserId){
                        this._chatView.addMessage(message.message, true);
                    }
                    else{
                        this._chatView.addMessage(message.message, false);
                    }
                });
            });
        });
    }


}

export default AppController;
