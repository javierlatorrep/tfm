/**
 * 
 */
class PanelSelectorController {
    /**
     * 
     * @param {*} param0 
     */
    constructor({ panelSelectorView }) {
        this._panelSelectorView = panelSelectorView;
    }

    /**
     * 
     */
    start() {
        this._setupPanelSelection();
    }

    /**
     * 
     */
    _setupPanelSelection() {
        this._panelSelectorView.start();
    }


}

export default PanelSelectorController;
