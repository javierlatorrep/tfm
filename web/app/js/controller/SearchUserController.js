/**
 * 
 */
class SearchUserController {
    /**
     * CONSTANTS
     */
    static get SEARCH_KEYUP_TIME() { return 1000 }

    /**
     * 
     * @param {*} appController 
     * @param {*} searchUserModel 
     * @param {*} searchUserView 
     * @param {*} chatView 
     * @param {*} chatModel 
     * @param {*} username 
     */
    constructor(appController, searchUserModel, searchUserView, chatView, chatModel, username) {
        this._appController     = appController;
        this._searchUserModel   = searchUserModel;
        this._searchUserView    = searchUserView;
        this._chatModel         = chatModel;
        this._chatView          = chatView;
        this._username          = username;
        this.searchFunction = null;
    }

    /**
     * 
     */
    start() {
        this._setupUserView();
    }

    /**
     * Configura la vista de chat de un usuario
     * @private
     */
    _setupUserView() {
        this._searchUserView._setupUserSelection();
        this._searchUserView.on('userSelected', (data) => this._onUserSelected(data));

        this._searchUserView._setupInputValueChange();
        this._searchUserView.on('inputValueChange', (searchQuery) => this._onInputValueChange(searchQuery));
    }

    /**
     * Gestiona la selección de un usuario para abrir panel de chat
     * @private
     */
    _onUserSelected(id) {

        this._searchUserModel.find(id, (userstr) => {
            var user = userstr;

            if (typeof user === 'string') {
                user = JSON.parse(user)[0];
            }
            if (user && user.active) {
                this._appController.emit('create_offer', user);
            }
        });
    }

    /**
     * Gestiona la introducción de texto al buscar usuario
     * @private
     */
    _onInputValueChange(searchQuery) {
        clearTimeout(this.searchFunction);

        this.searchFunction = setTimeout(() => {
            this._searchUserModel.searchUsers(searchQuery, (users) => {
                this._searchUserView.listUsers(JSON.parse(users));
            });
        }, SearchUserController.SEARCH_KEYUP_TIME);
    }
}

export default SearchUserController;
