/**
 * 
 */
class ChatController {
    /**
     * CONSTANTS
     */
    static get SERVER() { return "ws://192.168.1.101:9090" }
    static get OFFER_TYPE_CALL() { return "call" }
    static get OFFER_TYPE_CHAT() { return "chat" }
    static get OFFER_TYPE_CLOSE_CALL() { return "close_call" }
    static get TIME_INTERVAL_ACTIVE_USER() { return 10000 }
    static get ICE_SERVERS() {
        return [{
            "urls": "stun:stun.1.google.com:19302" 
        }];
    }
    static get DATA_CHANNEL_OPTIONS() {
        return {
            ordered : true,
            reliable: true
        }
    }  
    static get RTCPEERCONNECTION_CONFIGURATION() {
        return {
            "iceServers": ChatController.ICE_SERVERS
        }
    }
    static get GET_USER_MEDIA_CONSTRAINTS() {
        return { 
            video: true, 
            audio: true 
        }
    }
    static get OFFER_OPTIONS() {
        return {
            offerToReceiveAudio: 0,
            offerToReceiveVideo: 1
        }
    }

    /**
     * 
     * @param {*} appController 
     * @param {*} mainAppView 
     * @param {*} appView 
     * @param {*} chatView 
     * @param {*} chatModel 
     * @param {*} username 
     */
    constructor(appController, mainAppView, appView, chatView,chatModel,searchUserModel,username) {
        this._appController     = appController;
        this._mainAppView       = mainAppView;
        this._appView           = appView;
        this._chatView          = chatView;
        this._chatModel         = chatModel;
        this._searchUserModel   = searchUserModel;
        this.dataChannel        = null;
        this.connectedUsername  = null;
        this.connectedUser      = null;
        this.connection         = new WebSocket(ChatController.SERVER);
        this.RTCPeerConnection  = null;
        this.stream             = null;
        this._username           = username;

        this._chatModel.setActiveUser();
        setInterval(
            this._chatModel.setActiveUser, 
            ChatController.TIME_INTERVAL_ACTIVE_USER
        );

        this._setupChatView();

        this._appController.on('create_offer', (user) => {
            this.connectedUser      = user;
            this.connectedUsername  = user.email;

            this.createNewOffer(ChatController.OFFER_TYPE_CHAT);
        });

        this._mainAppView.on("end_call", () => {
            this.stopCall();
            this.createNewOffer(ChatController.OFFER_TYPE_CLOSE_CALL);
        });

        this._chatView.on("start_call", () => this.onStartCall());
        this._chatView.on('message_sent', (data) => this.onMessageSent(data));

        this.connection.onopen = () => {
            console.info("Connected now to server " + ChatController.SERVER);

            if(this._username) {
                this.send({
                    type: "login",
                    name: this._username
                });
            }
        };

        this.connection.onerror = (err) => {
            console.error("Got error", err);
        };

        this.connection.onmessage = (message) => {
            var data = JSON.parse(message.data);

            switch(data.type) {
                case "login":
                    this.onLogin(data.success);

                    break;
                case "offer":
                    if (
                        data.offerType === ChatController.OFFER_TYPE_CALL       || 
                        data.offerType === ChatController.OFFER_TYPE_CLOSE_CALL || 
                        confirm(data.name + " is trying to open a chat with you. Do you accept?")
                    )
                        this.onOffer(data.offer, data.name, data.offerType);

                    break;
                case "answer":  
                    this.onAnswer(data.answer, data.answerType);

                    break;
                case "candidate":
                    this.onCandidate(data.candidate);
                    
                    break;
                case "leave":
                    alert('Connection has been lost.');
                    window.location.reload();
                    
                    break;
            }
        }
    }

    /**
     * 
     */
    addDevicesTracks() {
        //displaying local video stream on the page
        this._mainAppView.setLocalVideoSrcObject(this.stream);

        // setup stream listening
        if (this.hasAddTrack) {
            this.stream.getTracks().forEach((track) => {
                this.RTCPeerConnection.addTrack(track, this.stream);
            });
        } else {
            this.RTCPeerConnection.addStream(this.stream);
        }
    }

    /**
     * 
     */
    stopCall() {
        this.stream.getAudioTracks().forEach((track) => {
            track.stop();
            this.stream.removeTrack(track);
        });

        this.stream.getVideoTracks().forEach((track) => {
            track.stop();
            this.stream.removeTrack(track);
        });

        this._mainAppView.setLocalVideoSrcObject(null);
        this._mainAppView.setRemoteVideoSrcObject(null);

        if (this.hasAddTrack) {
            this.RTCPeerConnection.getSenders().forEach((sender) => {
                this.RTCPeerConnection.removeTrack(sender);
            });
        } else {
            this.RTCPeerConnection.removeStream(this.stream);
        }
    }

    /**
     * 
     * @param {*} offerType 
     */
    createNewOffer(offerType) {
        this.RTCPeerConnection.createOffer(
            ChatController.OFFER_OPTIONS
        ).then((offer) => {
            this.send({
                type: "offer",
                offer: offer,
                offerType: offerType
            });

            this.RTCPeerConnection.setLocalDescription(offer);
        }).catch((error) => {
            alert("An error has occurred when creating new offer.");
        });
    }

    /**
     * 
     * @param {*} offerType 
     */
    createNewAnswer(offerType) {
        this.RTCPeerConnection.createAnswer().then((answer) => {
            this.RTCPeerConnection.setLocalDescription(answer);

            this.send({
                type: "answer",
                answer: answer,
                answerType: offerType
            });
        }).catch((error) => console.error("oops...error occurred when responding offer."));
    }

    /**
     * 
     */
    getConnectedUser() {
        return this.connectedUsername;
    }

    /**
     * 
     */
    getConnection() {
        return this.connection;
    }

    /**
     * 
     * @param {*} answer 
     * @param {*} answerType 
     */
    onAnswer(answer, answerType) {
        this.RTCPeerConnection.setRemoteDescription(new RTCSessionDescription(answer));

        if (
            answerType === ChatController.OFFER_TYPE_CHAT ||
            answerType === ChatController.OFFER_TYPE_CLOSE_CALL
        ) {
            this.openChat();
        }
    }

    /**
     * 
     * @param {*} candidate 
     */
    onCandidate(candidate) {
        this.RTCPeerConnection.addIceCandidate(new RTCIceCandidate(candidate));
    }

    /**
     * 
     * @param {*} success 
     */
    onLogin(success) {
        if (success === false) {
            alert("Something went wrong. Please, try again.");
        } else {
            //creating our RTCPeerConnection object
            this.RTCPeerConnection = new RTCPeerConnection(
                ChatController.RTCPEERCONNECTION_CONFIGURATION
            );

            console.log("RTCPeerConnection object was created");
            console.log(this.RTCPeerConnection);

            this.hasAddTrack = this.RTCPeerConnection.addTrack !== undefined;

            // When a remote user adds stream to the peer connection, we display it
            if (this.hasAddTrack) {
                this.RTCPeerConnection.ontrack = (evt) => {
                    this.openCall(evt.streams[0]);
                };
            } else {
                this.RTCPeerConnection.onaddstream = (evt) => {
                    this.openCall(evt.stream);
                };
            }

            // Setup ice handling
            // When the browser finds an ice candidate we send it to another peer
            this.RTCPeerConnection.onicecandidate = (event) => {
                if (event.candidate) {
                    this.send({
                        type: "candidate",
                        candidate: event.candidate
                    });
                }
            };

            this.RTCPeerConnection.ondatachannel = (event) => this.receiveChannel(event);

            this.openDataChannel();
        }
    }

    /**
     * 
     * @param {*} offer 
     * @param {*} name 
     * @param {*} offerType 
     */
    onOffer(offer, name, offerType) {
        this.connectedUser = {
            "email": name,
            "active": true
        };
        this.connectedUsername = name;
        this.RTCPeerConnection.setRemoteDescription(new RTCSessionDescription(offer));

        if (offerType === ChatController.OFFER_TYPE_CALL) {
            navigator.mediaDevices.getUserMedia(
                ChatController.GET_USER_MEDIA_CONSTRAINTS
            ).then((stream) => {
                this.stream = stream;

                this.addDevicesTracks();
                this.createNewAnswer(offerType);
            }).catch((error) => console.error("oops...error occurred getting user media."));
        } else {
            if (offerType === ChatController.OFFER_TYPE_CLOSE_CALL)
                this.stopCall();

            this.createNewAnswer(offerType);
            this.openChat();

        }
    }

    /**
     * 
     */
    openDataChannel() {
        this.dataChannel = this.RTCPeerConnection.createDataChannel(
            "myDataChannel",
            ChatController.DATA_CHANNEL_OPTIONS
        );

        this.dataChannel.onerror    = (error) => { console.error("Error:", error) };
        this.dataChannel.onmessage  = (event) => this.onReceiveMessage(event);
    }

    /**
     * 
     * @param {*} message 
     */
    send(message) {
        var connectedUser = this.getConnectedUser();

        if (connectedUser) {
            message.name = connectedUser;
        }

        this.connection.send(JSON.stringify(message));
    }

    /**
     * Gestiona un mensaje de chat introducido por el usuario
     * @private
     */
    onMessageSent(message) {
        if (this.dataChannel.readyState === "open") {
            this.dataChannel.send(message);

            this._chatView.addMessage(message);
            this._chatView.resetInputValue();
            this._chatView.disableSendButton();
            this._chatModel.save(this.openChatId,message,this._username,this.connectedUsername, (historyId) => this._setupLastHistoryId(historyId));
        } else {
            alert("Datachanned is not open.");
            console.error("Datachannel is " + this.dataChannel.readyState);
        }
    }

    /**
     * 
     */
    onStartCall() {
        navigator.mediaDevices.getUserMedia(
            ChatController.GET_USER_MEDIA_CONSTRAINTS
        ).then((stream) => {
            this.stream = stream;
            this.addDevicesTracks();
            this.createNewOffer(ChatController.OFFER_TYPE_CALL);
        }).catch((error) => {
            alert("There is not media to stream. Connect your webcam to pc.");
            console.error(error) 
        });
    }

    /**
     * 
     * @param {*} event 
     */
    onReceiveMessage(event) {
        this._chatView.addMessage(event.data, true);
    }

    /**
     * 
     * @param {*} stream 
     */
    openCall(stream) {
        this._mainAppView.setRemoteVideoSrcObject(stream);
        this._mainAppView.showCallPanel();
    }

    /**
     * abre canal
     */
    openChat() {
        if (this.connectedUser) {
            this._mainAppView.showAppPanel();
            this._chatView.showContact(this.connectedUser);
            //this._appView.showPanel(document.querySelector("#js-chat"));
            this._chatModel.openChat(this._username,this.connectedUsername, (chatId) => this._setupChatId(chatId) );

            this._searchUserModel.findByEmail(this.connectedUsername, (users) => {
                var connectedUser = JSON.parse(users)[0].id;
                this._appController._showChatsWith(connectedUser);
            });
        }
    }

    /**
     * 
     * @param {*} event 
     */
    receiveChannel(event) {
        this.dataChannel = event.channel;
        this.dataChannel.onmessage = this.onReceiveMessage;
        // this.dataChannel.onopen = handleReceiveChannelStatusChange;
        // this.dataChannel.onclose = handleReceiveChannelStatusChange;
    }

    /**
     * Configura la vista de chat de un usuario
     * @private
     */
    _setupChatView() {
        this._chatView._setupInputMessage();
        this._chatView._setupInputValueChange();
    }

    /**
     * Configura el chad ID que se ha abierto
     * @private
     */
    _setupChatId(chatId) {
        console.log("Open chat =>",chatId);
        this.openChatId = chatId;
    }

    /**
     * Configura el history ID del último mensaje enviado
     * @private
     */
    _setupLastHistoryId(historyId) {
        console.log("History message id =>",historyId);
        this.historyId = historyId;
    }
}

export default ChatController;
