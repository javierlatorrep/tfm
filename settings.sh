#!/usr/bin/env bash

sudo service mysql restart

sudo apt remove cmdtest -y
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

echo "installing yarn"
sudo apt-get update && sudo apt-get install yarn

echo "installing browserify"
sudo npm install browserify -g
sudo apt autoremove

echo "browserifying app/hs/boot.js"
sudo browserify /var/www/html/current/web/app/js/boot.js -o dist/index.js -t [ babelify --presets [ es2015 ] ]

echo "Starting signaling-server"
#sudo node /var/www/html/current/signaling-server/index.js
