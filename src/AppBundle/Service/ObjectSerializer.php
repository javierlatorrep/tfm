<?php

namespace AppBundle\Service;

use Symfony\Component\Serializer\Serializer;

class ObjectSerializer
{
    const JSON = "json";

    private $serializer;

    public function __construct(Serializer $serializer)
    {
        $this->serializer = $serializer;
    }

    public function JSONSerialize($objects)
    {
        return $this->serializer->serialize($objects, self::JSON);
    }

    public function JSONDeserialize($data, $className)
    {
        return $this->serializer->deserialize($data, $className, self::JSON);
    }
}
