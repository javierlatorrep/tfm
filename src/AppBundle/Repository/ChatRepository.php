<?php
namespace AppBundle\Repository;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class ChatRepository extends EntityRepository
{
    public function findAllUsersIHaveChattedWith(User $localUser)
    {
        return $this->createQueryBuilder("chat")
            ->select('user.id,user.email,user.active')
            ->where ('chat.transmitter = :local_user' )
            ->join('chat.receiver', 'user')
            ->setParameter("local_user", $localUser)
            ->groupBy("user.id")
            ->getQuery()
            ->execute();
    }
}
