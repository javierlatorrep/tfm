<?php
namespace AppBundle\Repository;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class HistoryRepository extends EntityRepository
{
    public function findAllChatsBetweenUsers(User $localUser, User $remoteUser)
    {
        return $this->createQueryBuilder("history")
            ->select("history.message,transmitter.id as transmitter_id")
            ->where ('((history.transmitter = :local_user and history.receiver = :remote_user) or (history.transmitter = :remote_user and history.receiver = :local_user))' )
            ->setParameter("local_user", $localUser)
            ->join('history.transmitter', 'transmitter')
            ->join('history.receiver', 'receiver')
            ->setParameter("remote_user", $remoteUser)
            ->orderBy("history.date")
            ->getQuery()
            ->execute();
    }
}
