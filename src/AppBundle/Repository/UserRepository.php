<?php
namespace AppBundle\Repository;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    public function findAllUsersFilteredByQueryStringOrderedByEmail($query, User $user)
    {
        $email = $user->getEmail();

        return $this->createQueryBuilder("user")
            ->select("user.id, user.email, user.active")
            ->andWhere("user.email LIKE :query")
            ->setParameter("query", "%" . $query . "%")
            ->andWhere("user.email != :email")
            ->setParameter("email", $email)
            ->orderBy("user.email")
            ->getQuery()
            ->execute();
    }

    public function findUser($id)
    {
        return $this->createQueryBuilder("user")
            ->select("user.id,user.email, user.active")
            ->andWhere("user.id = :id")
            ->setParameter("id", $id)
            ->getQuery()
            ->execute();
    }

    public function findUserByEmail($email)
    {
        return $this->createQueryBuilder("user")
            ->select("user.id")
            ->andWhere("user.email= :email")
            ->setParameter("email", $email)
            ->getQuery()
            ->execute();
    }

    public function findNotActiveUsersSinceSeconds($seconds)
    {
        $time = new \DateTime();
        $time->modify("-" . $seconds . ' second');

        return $this
            ->createQueryBuilder("user")
            ->select("user")
            ->andWhere("user.lastTimeActive <= :time")
            ->andWhere("user.active = :active")
            ->setParameter("time", $time)
            ->setParameter("active", 1)
            ->getQuery()
            ->execute();
    }
}
