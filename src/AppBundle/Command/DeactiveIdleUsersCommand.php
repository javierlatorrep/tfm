<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DeactiveIdleUsersCommand extends ContainerAwareCommand
{
    private $entityManager;
    private $userRepository;

    protected function configure()
    {
        $this
            ->setName('app:desactive-idle-users')
            ->setDescription('Set active attribute to false.')
            ->setHelp('Deactive users that have not reset active attribute for DESACTIVE_TIME period.');
    }

    protected function initialize(InputInterface $input, OutputInterface $output) {
        $this->entityManager = $this->getContainer()->get('doctrine')->getManager();
        $this->userRepository = $this->getContainer()->get('doctrine')->getRepository('AppBundle:User');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $users = $this->userRepository->findNotActiveUsersSinceSeconds(15);

        foreach($users as $user) {
            $user->setActive(0);
            $output->writeln(['Deactived user ' . $user->getUsername() . " Active => " . $user->getActive() ]);
        }

        $this->entityManager->flush();
    }
}
