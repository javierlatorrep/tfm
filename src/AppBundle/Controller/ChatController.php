<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Chat;
use AppBundle\Entity\History;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ChatController extends Controller
{

    /**
     * @Route("/openChat", name="openChat", options={"expose"=true})
     * @Method({"POST"})
     *
     */
    public function openChat (Request $request)
    {
        $localUserEmail = $request->request->get('localUsername');
        $remoteUserEmail = $request->request->get('connectedUsername');
        if (!$this->get('security.authorization_checker')->isGranted(User::ROLE_USER)) {
            return $this->redirectToRoute('security_login');
        }

        $idList = $this->getDoctrine()->getRepository('AppBundle:User')
            ->findUserByEmail($localUserEmail);

        $localUserId = 0;
        foreach ($idList as $key=>$value){
            $localUserId = $value['id'];
        }

        $localUser = $this->getDoctrine()
            ->getRepository(User::class)
            ->find($localUserId);


        $idList = $this->getDoctrine()->getRepository('AppBundle:User')
            ->findUserByEmail($remoteUserEmail);

        $remoteUserId = 0;
        foreach ($idList as $key=>$value){
            $remoteUserId = $value['id'];
        }

        $remoteUser = $this->getDoctrine()
            ->getRepository(User::class)
            ->find($remoteUserId);

        $em = $this->getDoctrine()->getManager();

        $chat = new Chat();
        $chat->setTransmitter($localUser);
        $chat->setReceiver($remoteUser);
        $date = new \DateTime();
        $chat->setDate($date);
        $em->persist($chat);
        $em->flush();

        $serializer = $this->container->get("app.object_serializer");
        return new JsonResponse($serializer->JSONSerialize($chat->getId()));
    }

    /**
     * @Route("/saveChatMessage", name="saveChatMessage", options={"expose"=true})
     * @Method({"POST"})
     *
     */
    public function saveChatMessage(Request $request)
    {
        $chatId = $request->request->get('chatId');
        $message = $request->request->get('message');
        $localUserEmail = $request->request->get('localUsername');
        $remoteUserEmail = $request->request->get('connectedUsername');
        if (!$this->get('security.authorization_checker')->isGranted(User::ROLE_USER)) {
            return $this->redirectToRoute('security_login');
        }

        $chat = $this
            ->getDoctrine()
            ->getRepository(Chat::class)
            ->find($chatId);

        $idList = $this->getDoctrine()->getRepository('AppBundle:User')
            ->findUserByEmail($localUserEmail);

        $localUserId = 0;
        foreach ($idList as $key=>$value){
            $localUserId = $value['id'];
        }

        $localUser = $this->getDoctrine()
            ->getRepository(User::class)
            ->find($localUserId);

        $idList = $this
            ->getDoctrine()
            ->getRepository('AppBundle:User')
            ->findUserByEmail($remoteUserEmail);

        $remoteUserId = 0;
        foreach ($idList as $key=>$value){
            $remoteUserId = $value['id'];
        }

        $remoteUser = $this
            ->getDoctrine()
            ->getRepository(User::class)
            ->find($remoteUserId);

        $em = $this->getDoctrine()->getManager();

        $history = new History();
        $history->setChat($chat);
        $history->setTransmitter($localUser);
        $history->setReceiver($remoteUser);
        $history->setMessage($message);
        $date = new \DateTime();
        $history->setDate($date);
        $em->persist($history);
        $em->flush();

        $serializer = $this->container->get("app.object_serializer");
        return new JsonResponse($serializer->JSONSerialize($history->getId()));
    }

    /**
     * @Route("/getChatsBetweenUsers", name="getChatsBetweenUsers", options={"expose"=true})
     * @Method({"POST"})
     *
     */
    public function getChatsBetweenUsers(Request $request)
    {
        $localUserEmail = $request->request->get('localUsername');
        $remoteUserEmail = $request->request->get('remoteUsername');
        if (!$this->get('security.authorization_checker')->isGranted(User::ROLE_USER)) {
            return $this->redirectToRoute('security_login');
        }

        $idList = $this
            ->getDoctrine()
            ->getRepository('AppBundle:User')
            ->findUserByEmail($localUserEmail);

        $localUserId = 0;
        foreach ($idList as $key=>$value){
            $localUserId = $value['id'];
        }

        $localUser = $this
            ->getDoctrine()
            ->getRepository(User::class)
            ->find($localUserId);

        $idList = $this
            ->getDoctrine()
            ->getRepository('AppBundle:User')
            ->findUserByEmail($remoteUserEmail);

        $remoteUserId = 0;
        foreach ($idList as $key=>$value){
            $remoteUserId = $value['id'];
        }

        $remoteUser = $this->getDoctrine()
            ->getRepository(User::class)
            ->find($remoteUserId);

        $allChatsBetweenUsers = $this
            ->getDoctrine()
            ->getRepository('AppBundle:History')
            ->findAllChatsBetweenUsers($localUser, $remoteUser)
        ;

        $serializer = $this->container->get("app.object_serializer");
        return new JsonResponse($serializer->JSONSerialize($allChatsBetweenUsers));
    }

    /**
     * @Route("/getUsersIHaveChattedWith", name="getUsersIHaveChattedWith", options={"expose"=true})
     * @Method({"POST"})
     *
     */
    public function getUsersIHaveChattedWith(Request $request)
    {
        $localUserEmail = $request->request->get('localUsername');

        if (!$this->get('security.authorization_checker')->isGranted(User::ROLE_USER)) {
            return $this->redirectToRoute('security_login');
        }

        $idList = $this
            ->getDoctrine()
            ->getRepository('AppBundle:User')
            ->findUserByEmail($localUserEmail);

        $localUserId = 0;
        foreach ($idList as $key=>$value){
            $localUserId = $value['id'];
        }

        $localUser = $this
            ->getDoctrine()
            ->getRepository(User::class)
            ->find($localUserId);

        $allUsersIHaveChattedWith = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Chat')
            ->findAllUsersIHaveChattedWith($localUser)
        ;

        $serializer = $this->container->get("app.object_serializer");
        return new JsonResponse($serializer->JSONSerialize($allUsersIHaveChattedWith));
    }
}
