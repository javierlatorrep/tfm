<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AppController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function indexAction(Request $request)
    {
        return $this->render('app/home.html.twig');
    }

    /**
     * @Route("/app", name="app")
     */
    public function appAction(Request $request)
    {
        return $this->render('app/app.html.twig');
    }
}
