<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\UserRegistrationForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * 
 */
class UserController extends Controller
{
    /**
     * @Route("/signup", name="user_signup")
     */
    public function registerAction(Request $request)
    {
        $form = $this->createForm(UserRegistrationForm::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $user->setActive(true);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            $this->addFlash('success', 'Welcome ' . $user->getEmail());

            return $this->get('security.authentication.guard_handler')
                ->authenticateUserAndHandleSuccess(
                    $user,
                    $request,
                    $this->get('app.security.login_form_authenticator'),
                    'main'
                );
        }

        return $this->render('user/register.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/users/{query}", name="users", options={"expose"=true})
     */
    public function getUsers($query, UserInterface $user)
    {
        if (!$this->get('security.authorization_checker')->isGranted(User::ROLE_USER)) {
            return $this->redirectToRoute('security_login');
        }

        $users = $this->getDoctrine()->getRepository('AppBundle:User')
            ->findAllUsersFilteredByQueryStringOrderedByEmail($query, $user)
        ;

        $serializer = $this->container->get("app.object_serializer");
        $JsonUsers = $serializer->JSONSerialize($users);

        return new JsonResponse($JsonUsers);
    }

    /**
     * @Route("/user/{id}", name="findUser", options={"expose"=true})
     */
    public function find($id)
    {
        if (!$this->get('security.authorization_checker')->isGranted(User::ROLE_USER)) {
            return $this->redirectToRoute('security_login');
        }

        $user = $this->getDoctrine()->getRepository('AppBundle:User')
            ->findUser($id)
        ;

        $serializer = $this->container->get("app.object_serializer");
        $JsonUsers = $serializer->JSONSerialize($user);

        return new JsonResponse($JsonUsers);
    }

    /**
     * @Route("/findUserByEmail/{email}", name="findUserByEmail", options={"expose"=true})
     */
    public function findUserByEmail($email)
    {
        if (!$this->get('security.authorization_checker')->isGranted(User::ROLE_USER)) {
            return $this->redirectToRoute('security_login');
        }

        $user = $this
            ->getDoctrine()
            ->getRepository('AppBundle:User')
            ->findUserByEmail($email);

        $serializer = $this->container->get("app.object_serializer");
        $result = $serializer->JSONSerialize($user);

        return new JsonResponse($result);
    }

    /**
     * @Route("/active", name="activeUser", options={"expose"=true})
     * @Method("GET")
     */
    public function activeUser()
    {
        if (!$this->get('security.authorization_checker')->isGranted(User::ROLE_USER)) {
            return $this->redirectToRoute('security_login');
        }

        $entityManager = $this->getDoctrine()->getManager();

        $user = $this->getUser();
        $user->setActive(true);
        $user->setLastTimeActive();

        $entityManager->flush();

        return new Response();
    }
}
