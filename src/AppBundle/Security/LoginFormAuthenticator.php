<?php

namespace AppBundle\Security;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Util\TargetPathTrait;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Security;
use AppBundle\Form\LoginForm;

class LoginFormAuthenticator extends AbstractFormLoginAuthenticator
{
    use TargetPathTrait;

    private $formFactory;
    private $entityManager;
    private $router;
    private $passwordEncoder;

    public function __construct(
        FormFactoryInterface $formFactory,
        EntityManager $entityManager,
        RouterInterface $router,
        UserPasswordEncoder $passwordEncoder
    ) {
        $this->formFactory = $formFactory;
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->passwordEncoder = $passwordEncoder;
    }

    public function getCredentials(Request $request)
    {
        $isLoginSubmit = $request->attributes->get('_route') === 'security_login' && $request->isMethod('POST');

        if (!$isLoginSubmit) {
            return;
        }

        $form = $this->formFactory->create(LoginForm::class);
        $form->handleRequest($request);

        $data = $form->getData();
        $request->getSession()->set(
            Security::LAST_USERNAME,
            $data['_username']
        );

        return $data;
    }
    
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        return $this->entityManager->getRepository('AppBundle:User')
            ->findOneBy(['email' => $credentials['_username']]);
    }
    
    public function checkCredentials($credentials, UserInterface $user)
    {
        $password = $credentials['_password'];
        $loggedIn = false;

        if ($this->passwordEncoder->isPasswordValid($user, $password)) {
            $loggedIn = true;
        }

        return $loggedIn;
    }

    protected function getLoginUrl()
    {
        return $this->router->generate('security_login');
    }
    
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        // if the user hits a secure page and start() was called, this was
        // the URL they were on, and probably where you want to redirect to
        $targetPath = $this->getTargetPath($request->getSession(), $providerKey);
  
        if (!$targetPath) {
             $targetPath = $this->router->generate('app');
        }
 
        return new RedirectResponse($targetPath);
    }
}
